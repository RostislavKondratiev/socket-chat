
$(function () {
    var socket = io();
    let user;
    let data = { user: 'Anonymous',  message: '' };

    function isMine(user, message) {
        if(user === message.user) {
            return 'message--mine'
        }
    }
    $('#user').on('change',function (e) {
        data.user = e.target.value;
        user = e.target.value;
        $('#user').prop('disabled', true);
    });
    $('#message-form').submit(function(e){
        e.preventDefault();
        if($('#message_input').val().length > 0) {
            data.message = $('#message_input').val();
            socket.emit('chat message', data);
            $('#message_input').val('');
            return false;
        }
    });
    $('#imagefile').on('change', function(e){
        let file = e.originalEvent.target.files[0],
            reader = new FileReader();
        reader.onload = function(evt){
            let data = {
                user: user || 'Anonymous',
                image: evt.target.result
            };
            console.log('emit');
            socket.emit('chat image', data);
        };
        reader.readAsDataURL(file);
    });

    socket.on('count', function (msg) {
        $('#connected').text(msg);
    });

    socket.on('chat image', function (msg) {
        console.log('getted');
        let imageTemplate = Handlebars.compile(
            `<li class="message ${isMine(user, msg)}">
                            <div class="message-header">
                                <span class="message__author"> ${msg.user} </span>
                            </div>
                            <img src="${msg.image}" class="message__img">
                        </li>`
        )
        $('.messages').append(imageTemplate(true));
        window.scrollTo(0, document.body.scrollHeight);
    });

    socket.on('chat message', function(msg){
        console.log(msg);
        let messageTemplate = Handlebars.compile(
            `<li class="message ${isMine(user, msg)}">
                            <div class="message-header">
                                <span class="message__author"> ${msg.user} </span>
                            </div>
                            <div class="message__text">
                                ${msg.message}
                            </div>
                        </li>`
        );
        $('.messages').append(messageTemplate(true));
        window.scrollTo(0, document.body.scrollHeight);
    });
});