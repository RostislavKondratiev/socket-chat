const app = require('express')();
const express = require('express');
const http = require('http').Server(app);
const io = require('socket.io')(http);

app.get('/', function(req, res){
    res.sendFile(__dirname + '/app/index.html');
});

app.use(express.static(__dirname + '/app'));

let connected = 0;

io.on('connection', function(socket){
    connected++;
    io.emit('count',connected);
    socket.on('disconnect', function(){
        io.emit('count', --connected);
    });
    socket.on('chat message', function(msg){
        io.emit('chat message', msg);
    });
    socket.on('chat image', function (msg) {
        io.emit('chat image', msg);
    })
});

http.listen(8080, function(){
    console.log('listening on *:8080');
});